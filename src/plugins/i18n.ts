import {createI18n} from 'vue-i18n';
import fr from '@/locales/fr.json';

let locale = (import.meta.env.NODE_ENV as  string) === 'test'
    ? (import.meta.env.VITE_APP_I18N_LOCALE as string) || 'fr'
    : navigator.language.split('-')[0] || (import.meta.env.VITE_APP_I18N_LOCALE as string) || 'fr'

export default createI18n({
    locale,
    fallbackLocale: 'fr',
    messages: {fr},
})
